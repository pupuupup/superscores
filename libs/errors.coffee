module.exports =
  missed_key: (key) -> "#{key} is required"
  mismatched_key: (key) -> "#{key} mismatched"
  invalid_regex: (key) -> "Invalid #{key} format"
  duplicate_key: (key) -> "Duplicate #{key}"
  not_found: (key) -> "#{key} not found"
  bad_implement: (msg='') -> "Bad Implementation #{msg}"
  timeout: (msg='') -> "Gateway Timeout #{msg}"
  too_many: (sec) -> " too many request, try again in #{sec}"

  INTERNAL_ERROR: 'Internal Server Error'
  NOT_IMPLEMENT: 'Service is not yet Implement'
  AUTH_FAIL: 'Authentication failed'
  FORBIDDEN: 'Forbidden'
  UNAVAILABLE: 'Service Unavailable'
