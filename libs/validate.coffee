class Validate

  joi: (params, schema, opts={}) ->
    opts.no_code = opts.no_code or false
    options =
      abortEarly: false
      convert: true
      allowUnknown: false
      stripUnknown: false
    Joi.validateAsync(params, schema, options)
      .then (obj) => obj
      .catch (err) =>
        errs = _.map(err.details, (e) =>
          key = _.compact(e.path.split('.'))[0]
          _suffix =  switch e.type.split('.')[-1..][0]
            when 'required' then 'required'
            when 'allowUnknown' then 'is not allowed'
            else 'invalid format'
          "#{key} is #{_suffix}")
        unless opts.no_code
          throw [400, errs]
        else
          throw _.flattenDeep [errs]

module.exports = new Validate()
