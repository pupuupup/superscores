class Helper

  deepEscape: (data) =>
    recursiveData = data
    if _.isString(data) then recursiveData = escape data
    else if _.isObject(data)
      _.forEach recursiveData, (v, k) =>
         if _.isObject(v)
           recursiveData[k] = deepEscape v
    recursiveData

  mapWhere: (where, schemas) =>
    whereArr = _.filter _.keys(where), (v) => v in _.keys(schemas)
    arr = []
    _.forEach where, (v, k) =>
      arr.push "#{k}=\'#{v}\'"
    if _.isEmpty(arr)
      ''
    else
      "WHERE #{arr.join(' AND ')}"

  responseSuccess: (req, res, data) ->
    data = _.toArray(data)
    if data.length is 2
      res.status(data[0]).json(data[1])
    else if data[0] is 204
      res.status(204).end()
    else
      @responseError req, res, []

  responseError: (req, res, err) =>
    error = _.toArray(err)
    if error.length is 2 then res.status(error[0]).json(error: error[1])
    else res.sendStatus(500)

  toPascal: (text, token = '_') ->
    (_.capitalize(word) for word in text.split(token)).join ''


module.exports = new Helper()
