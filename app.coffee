express = require 'express'
bodyParser = require 'body-parser'
useragent = require 'express-useragent'
logger = require 'morgan'

global._ = require 'lodash'
global.config = require './config'

mysql = require 'mysql'
global.sql = mysql.createConnection(_.merge(config.mysql, {multipleStatements: true}))
sql.connect()


global.Promise = require 'bluebird'
global.sql = Promise.promisifyAll(sql)
global.Joi = Promise.promisifyAll(require 'joi')
global.fs = require 'fs'
global.Q = require 'q'
global.moment = require 'moment'
global.crypto = require 'crypto'
global.stringify = require 'json-stable-stringify'


# load all models
_.chain(fs.readdirSync './models')
  .filter (file) -> /coffee$/.test file
  .map (file) -> file.split('.coffee')[0]
  .forEach (file) -> global[_.capitalize(_.camelCase(file))] = require "./models/#{file}"
  .commit()

# load all libs
_.chain(fs.readdirSync './libs')
  .filter (file) -> /coffee$/.test file
  .map (file) -> file.split('.coffee')[0]
  .forEach (file) -> global[_.capitalize(_.camelCase(file))] = require "./libs/#{file}"
  .commit()

module.exports = (options={}) ->

  app = express()
  server = require('http').createServer app

  app.disable 'x-powered-by'
  app.set 'trust proxy', true
  app.set 'port', process.env.PORT or 3000
  app.set 'options', options

  app.use bodyParser.urlencoded(extended: true)
  app.use bodyParser.json()
  app.use useragent.express()

  # inject start time for tracking request-time
  app.use (req, res, next) ->
    req.start = Date.now()
    next()

  if app.get('env') in 'development'
    app.use logger 'dev'

  # load all controllers
  _.chain(fs.readdirSync('./controllers')
    .filter (file) ->
      stat = fs.statSync "./controllers/#{file}"
      stat.isDirectory())
    .forEach (dir) ->
      _.chain(fs.readdirSync("./controllers/#{dir}"))
        .filter (file) -> /coffee$/.test file
        .map (file) -> file.split('.coffee')[0]
        .forEach (file) ->
          _file = "#{dir}_#{file}"
          mod = "#{Helper.toPascal(_file)}Controller"
          global[mod] = require "./controllers/#{dir}/#{file}"
        .commit()
    .commit()


  # load router
  require('./controllers')(app)

  # Export HTTP Server or App constructor
  if options.server isnt false
    server.listen app.get('port'), ->
      unless SILENT
        console.log "http://localhost:#{app.get('port')}"
    app.server = server
    return app
  else
    return app
