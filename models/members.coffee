class Members

  get: (where, opts={}) =>
    where = Helper.deepEscape where, @schema
    criteria = Helper.mapWhere where
    sql.queryAsync("SELECT * FROM Members #{criteria}")

  create: (data) =>
    data = @mapData data
    @validate(data)
      .then (data) =>
        values = _.map(_.values(data), (v) => "\'#{v}\'").join(',')
        keys = _.keys(data).join(',')
        sql.queryAsync("INSERT INTO Members (#{keys}) VALUES (#{values})")
      .then => data

  mapData: (data) =>
    now = moment().format('YYYY-MM-DD HH:mm:ss')
    {
      firstName: data.firstName
      lastName: data.lastName
      createdAt: now
      updatedAt: now
    }

  schemas: => ['memberId', 'firstname', 'lastname']

  validate: (data) =>
    schema = Joi.object().keys
      firstName: Joi.string().required()
      lastName: Joi.string().required()
      createdAt: Joi.string().regex(Regex.date).required()
      updatedAt: Joi.string().regex(Regex.date).required()
    Promise.try(-> Validate.joi(data, schema, {no_code: true}))


module.exports = new Members()
