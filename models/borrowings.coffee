class Borrowings

  get: (where, opts={}) =>
    opts.link = opts.link or false
    where = Helper.deepEscape where, @schema
    criteria = Helper.mapWhere where
    sql.queryAsync("SELECT * FROM Borrowing #{criteria}")
      .then (data) =>
        if opts.link
          @link _.flattenDeep([data, data])
        else
          data

  #link will be more efficient with memory caching
  link: (data) =>
    Promise.resolve(data)
      .map (_data) =>
        Promise.all [
          Members.get(memberId: _data.memberId)
          Staffs.get(staffId: _data.staffId)
          Books.get(bookId: _data.bookId)
        ]
      .then (result) =>
        _.forEach data, (obj) =>
          _.forEach result, (v) =>
            obj.member = v[0][0]
            obj.staff = v[1][0]
            obj.book = v[2][0]
        data

  create: (data) =>
    data = @mapData data
    @validate(data)
      .then (data) =>
        values = _.map(_.values(data), (v) => "\'#{v}\'").join(',')
        keys = _.keys(data).join(',')
        sql.queryAsync("INSERT INTO Borrowing (#{keys}) VALUES (#{values})")
      .then => data

  returnBook: (id) =>
    unless _.isNumber(id) then throw false
    date = moment().format('YYYY-MM-DD HH:mm:ss')
    sql.queryAsync("UPDATE Borrowing
                    SET returnDate=\'#{date}\', updatedAt=\'#{date}'\
                    WHERE borrowId=\'#{id}\'")
      .then (updated) =>
        @get({borrowId: id}, {link: true})

  mapData: (data) =>
    now = moment().format('YYYY-MM-DD HH:mm:ss')
    {
      borrowDate: now
      returnDeadLine: moment().day(7).format('YYYY-MM-DD HH:mm:ss')
      memberId: data.memberId,
      bookId: data.bookId,
      staffId: data.staffId
      createdAt: now
      updatedAt: now
    }

  schemas: =>
      ['borrowId',
      'borrowDate',
      'returnDeadLine',
      'returnDate',
      'memberId',
      'bookId',
      'staffId']

  validate: (data) =>
    schema = Joi.object().keys
      borrowDate: Joi.string().regex(Regex.date).required()
      returnDeadLine: Joi.string().regex(Regex.date).required()
      returnDate: Joi.string().regex(Regex.date)
      memberId: Joi.number().integer().min(0).required()
      bookId: Joi.number().integer().min(0).required()
      staffId: Joi.number().integer().min(0).required()
      createdAt: Joi.string().regex(Regex.date).required()
      updatedAt: Joi.string().regex(Regex.date).required()
    Promise.try(-> Validate.joi(data, schema, no_code: true))

module.exports = new Borrowings()
