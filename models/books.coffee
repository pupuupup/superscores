class Books

  get: (where) =>
    where = Helper.deepEscape where
    criteria = Helper.mapWhere where, @schemas
    sql.queryAsync("SELECT * FROM Books #{criteria}")

  create: (data) =>
    data = @mapData data
    @validate(data)
      .then (data) =>
        values = _.map(_.values(data), (v) => "\'#{v}\'").join(',')
        keys = _.keys(data).join(',')
        sql.queryAsync("INSERT INTO Books (#{keys}) VALUES (#{values})")
      .then => data

  mapData: (data) =>
    {
      title: data.title
      year: data.year
      edition: data.edition
      publisher: data.publisher
      author: data.author
      addedDate: moment().format('YYYY-MM-DDTHH:mm:ss')
      createdAt: moment().format('YYYY-MM-DDTHH:mm:ss')
      updatedAt: moment().format('YYYY-MM-DDTHH:mm:ss')
    }

  schemas: =>
      ['bookId',
      'title',
      'year',
      'edition',
      'publisher',
      'author',
      'addedDate']

  validate: (data) =>
    schema = Joi.object().keys
      title: Joi.string().required()
      year: Joi.string().regex(Regex.year).required()
      edition: Joi.string().required()
      publisher: Joi.string().required()
      author: Joi.string().required()
      addedDate: Joi.string().regex(Regex.date).required()
      createdAt: Joi.string().regex(Regex.date).required()
      updatedAt: Joi.string().regex(Regex.date).required()
    Promise.try(-> Validate.joi(data, schema, {no_code: true}))

module.exports = new Books()
