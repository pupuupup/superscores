module.exports = (req, res) ->
  # update borrow
  #
  # @restapi PUT /member/:memberId/borrow
  # @return {String} 200 JSON

  schema = Joi.object().keys
    memberId: Joi.number().integer().min(0).required()
    bookId: Joi.number().integer().min(0).required()

  params = undefined

  Promise.try(-> Validate.joi(_.merge(req.params, req.body), schema))
    .then (p) ->
      params = p
      Promise.resolve(Borrowings.get({memberId: params.memberId, bookId: params.bookId}))
    .then (resultGet) =>
      if _.isEmpty(resultGet) then throw [404, Errors.not_found()]
      Promise.resolve(Borrowings.returnBook(_.first(resultGet).borrowId))
    .then (result) => [200, result]
    .then (data) -> Helper.responseSuccess req, res, data
    .catch (err) -> Helper.responseError req, res, err
