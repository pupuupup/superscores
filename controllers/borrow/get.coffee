module.exports = (req, res) ->
  # get borrow
  #
  # @restapi GET /member/:memberId/borrow
  # @return {String} 200 JSON

  schema = Joi.object().keys
    memberId: Joi.number().integer().min(0).required()

  params = undefined

  Promise.try(-> Validate.joi(req.params, schema))
    .then (p) ->
      params = p
      Promise.resolve(Members.get({memberId: params.memberId}))
    .then (member) =>
      if _.isEmpty(member) then throw [404, Errors.not_found('member not found')]
      Promise.resolve(Borrowings.get({memberId: params.memberId}, link:true))
    .then (result) => [200, result]
    .then (data) -> Helper.responseSuccess req, res, data
    .catch (err) -> Helper.responseError req, res, err
