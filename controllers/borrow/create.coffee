module.exports = (req, res) ->
  # create borrow
  #
  # @restapi POST /member/:memberId/borrow
  # @return {String} 200 JSON

  schema = Joi.object().keys
    memberId: Joi.number().integer().min(0).required()
    bookId: Joi.number().integer().min(0).required()
    staffId: Joi.number().integer().min(0).required()

  params = undefined

  Promise.try(-> Validate.joi(_.merge(req.params, req.body), schema))
    .then (p) ->
      params = p
      Promise.all [
        Promise.resolve(Members.get(memberId: params.memberId))
        Promise.resolve(Books.get(bookId: params.bookId))
        Promise.resolve(Staffs.get(staffId: params.staffId))
      ]
    .then (resultAll) =>
      _.forEach resultAll, (v) =>
        if _.isEmpty(v) then throw [404, Errors.not_found('member, book, or staff')]
      Promise.resolve(Borrowings.create(params))
    .then (result) => [200, result]
    .then (data) -> Helper.responseSuccess req, res, data
    .catch (err) -> Helper.responseError req, res, err
