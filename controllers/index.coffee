member = require '../models/members'

module.exports = (app) ->

  app.get('/member/:memberId/borrow', BorrowGetController)
  app.post('/member/:memberId/borrow', BorrowCreateController)
  app.put('/member/:memberId/borrow', BorrowReturnController)

  app.get '/', (req, res) ->
    res.send 'hi'

