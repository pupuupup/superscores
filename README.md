MY ENVIRONMENT
    
    node version 5.0.0
    npm version 3.3.9
    mysql 5.7.10

HOW TO RUN SERVER (development mode)

    git clone https://pupuupup@bitbucket.org/pupuupup/superscores.git
    cd superscores
    npm install
    node server.js
    
HOW TO RUN TEST SERVER (test mode)

    mocha
    
CONFIG FILE FOR MYSQL DATABASE

    You need to create 2 database in MYSQL one for test mode (mocha) and one for development mode
    Change user, password, host, database in config.coffee accordingly
    
    Warning:
        In test mode, test database will be dropped and re-created. Then it will import library.sql file to refresh everything.
        *Test database will be dropped. Please becareful.
        
TIME USED IN DEVELOPMENT

    5 days (around 3-5 hours a day)
    
        1 day: installation of MySQL + learn how to use mysql (driver and command line)
        1 day: set up project, connect to MySQL using node, setup mocha
        2 days: models and its test cases
        1 day: controllers and its test cases
    
    
API DOC (brief)

    GET /member/:memberId/borrow
        get all borrow history of a member
        request:    
            required data = {
                params:
                    memberId 
            }
        response:
            an object of borrow table with link to member, staff, and book
        
    POST /member/:memberId/borrow
        create borrow
        required data = {
            params:
                memberId
            body:
                bookId
                staffId
        }
        response:
            created object
        
    PUT /member/:memberId/borrow
        return a book
        required data = {
            params:
                memberId
            body:
                bookId
        }
        response:
            updated object