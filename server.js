var env = process.env.NODE_ENV || 'development'
global.SILENT = process.env.SILENT === '1'

require("coffee-script/register");
require('./app')();
