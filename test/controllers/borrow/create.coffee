require '../../setup'

describe 'Create Borrow Controller', =>

  before (done) =>
    @validData =
      bookId: '1'
      staffId: '1'
    done()

  it 'should successfully borrow', (done) =>
    memberId = 1
    Q()
      .then =>
        request.post("/member/#{memberId}/borrow")
          .send(@validData)
          .expect(200)
          .expect (res) =>
            expect(res.body.memberId).eql parseInt memberId
            expect(res.body.staffId).eql parseInt @validData.staffId
            expect(res.body.bookId).eql parseInt @validData.bookId
      .then => done()
      .fail (err) => console.log err

  it 'should not be able to borrow a book if member, staff, or book is not found', (done) =>
    memberId = 10000
    Q()
      .then =>
        request.post("/member/#{memberId}/borrow")
          .send(@validData)
          .expect(404)
          .expect (res) =>
            expect(res.statusCode).eql 404
            done()

  it 'should fail to borrow if data is invalid', (done) =>
    memberId = 'invalid data'
    Q()
      .then =>
        request.post("/member/#{memberId}/borrow")
          .send(@validData)
          .expect(400)
          .expect (res) =>
            expect(res.statusCode).eql 400
            done()

  it 'should fail to borrow if data is complete', (done) =>
    memberId = 'invalid data'
    Q()
      .then =>
        request.post("/member/#{memberId}/borrow")
          .send(_.omit(@validData, 'bookId'))
          .expect(400)
          .expect (res) =>
            expect(res.statusCode).eql 400
            done()
