require '../../setup'

describe 'Return Borrow Controller', =>

  before (done) =>
    @validData =
      bookId: '2'
      staffId: '1'
    done()

  it 'should successfully update borrow return date', (done) =>
    memberId = 1
    Q()
      .then =>
        request.post("/member/#{memberId}/borrow")
          .send(@validData)
          .expect(200)
          .expect (res) =>
            expect(res.body.memberId).eql parseInt memberId
            expect(res.body.staffId).eql parseInt @validData.staffId
            expect(res.body.bookId).eql parseInt @validData.bookId
      .then =>
        request.put("/member/#{memberId}/borrow")
          .send(_.omit(@validData, 'staffId'))
          .expect(200)
          .expect (res) =>
            expect(res.statusCode).eql 200
      .then => done()
      .fail (err) => console.log err

  it 'should fail for invalid data', (done) =>
    memberId = 'invalid data'
    Q()
      .then =>
        request.put("/member/#{memberId}/borrow")
          .send(_.omit(@validData, 'staffId'))
          .expect(400)
          .expect (res) =>
            expect(res.statusCode).eql 400
      .then => done()

  it 'should fail for not existed borrow data', (done) =>
    memberId = 100000
    Q()
      .then =>
        request.put("/member/#{memberId}/borrow")
          .send(_.omit(@validData, 'staffId'))
          .expect(404)
          .expect (res) =>
            expect(res.statusCode).eql 404
      .then => done()
