require '../../setup'

describe 'Get Borrow Controller', =>

  it 'should successfully get borrow', (done) =>
    memberId = 1
    Q()
      .then =>
        request.get("/member/#{memberId}/borrow")
          .expect(200)
          .expect (res) =>
            _.forEach res.body, (obj) =>
              expect(obj).to.have.property 'borrowId'
              expect(obj).to.have.property 'borrowDate'
              expect(obj).to.have.property 'returnDeadline'
              expect(obj).to.have.property 'returnDate'
              expect(obj).to.have.property 'createdAt'
              expect(obj).to.have.property 'updatedAt'
              expect(obj.member).to.be.an 'object'
              expect(obj.member).to.have.property 'firstName'
              expect(obj.member).to.have.property 'lastName'
              expect(obj).to.have.property 'member'
              expect(obj.staff).to.be.an 'object'
              expect(obj.staff).to.have.property 'firstName'
              expect(obj.staff).to.have.property 'lastName'
              expect(obj).to.have.property 'staff'
              expect(obj.book).to.be.an 'object'
              expect(obj.book).to.have.property 'bookId'
              expect(obj.book).to.have.property 'author'
              expect(obj.book).to.have.property 'year'
              expect(obj.book).to.have.property 'title'
              expect(obj).to.have.property 'book'
      .then => done()
      .fail (err) => console.log err

  it 'should fail for not existing merchant', (done) =>
    memberId = 100000000
    Q()
      .then =>
        request.get("/member/#{memberId}/borrow")
          .expect(404)
          .expect (res) =>
            expect(res.statusCode).eql 404
            done()
