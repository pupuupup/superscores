require '../setup'

describe 'Members Model', =>

  before (done) =>
    @validData =
      firstName: 'Pupu'
      lastName: 'Upup'
    done()

  it 'create successfully and get successfully', (done) =>
    validData = _.cloneDeep(@validData)
    Q.invoke(Members, 'create', validData)
      .then (resultCreate) =>
        expect(resultCreate.firstName).eql validData.firstName
        expect(resultCreate.lastName).eql validData.lastName
      .then =>
        Q.invoke(Members, 'get', {firstName: validData.firstName})
      .then (resultGet) =>
        resultGet = _.first resultGet
        expect(resultGet).to.have.property 'memberId'
        expect(resultGet.firstName).eql validData.firstName
        expect(resultGet.lastName).eql validData.lastName
        done()
      .fail (err) => console.log err

  it 'should return empty array for not found', (done) =>
    Q.invoke(Members, 'get', {firstName: 'should not be found'})
      .then (resultGet) =>
        expect(resultGet).eql []
        done()
      .fail (err) => console.log err
