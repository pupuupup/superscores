require '../setup'

describe 'Books Model', =>

  before (done) =>
    @validData =
      title: 'Python'
      year: '2014'
      edition: 'fifth'
      publisher: 'pupuupup'
      author: 'pupuupup'

    done()

  it 'create successfully and get successfully', (done) =>
    validData = _.cloneDeep(@validData)
    Q.invoke(Books, 'create', validData)
      .then (resultCreate) =>
        expect(resultCreate.title).eql validData.title
        expect(resultCreate.year).eql validData.year
        expect(resultCreate.edition).eql validData.edition
        expect(resultCreate.publisher).eql validData.publisher
        expect(resultCreate.author).eql validData.author
      .then =>
        Q.invoke(Books, 'get', {title: validData.title})
      .then (resultGet) =>
        resultGet = _.first resultGet
        expect(resultGet).to.have.property 'bookId'
        expect(resultGet.title).eql validData.title
        expect(resultGet.year).eql parseInt validData.year
        expect(resultGet.edition).eql validData.edition
        expect(resultGet.publisher).eql validData.publisher
        expect(resultGet.author).eql validData.author
        done()
      .fail (err) => console.log err

  it 'should return empty array for not found', (done) =>
    Q.invoke(Books, 'get', {title: 'should not be found'})
      .then (resultGet) =>
        expect(resultGet).eql []
        done()
      .fail (err) => console.log err
