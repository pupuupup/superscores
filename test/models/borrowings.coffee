require '../setup'

describe 'Borrowing Model', =>

  before (done) =>
    @validData = {
      memberId: 1,
      bookId: 1,
      staffId: 1
    }
    done()

  it 'create successfully and get successfully', (done) =>
    Q.invoke(Borrowings, 'create', @validData)
      .then (resultCreate) =>
        expect(resultCreate.memberId).eql @validData.memberId
        expect(resultCreate.bookId).eql @validData.bookId
        expect(resultCreate.staffId).eql @validData.staffId
      .then =>
        Q.invoke(Borrowings, 'get', borrowId: 1)
      .then (resultGet) =>
        expect(resultGet[0].memberId).eql @validData.memberId
        expect(resultGet[0].bookId).eql @validData.bookId
        expect(resultGet[0].staffId).eql @validData.staffId
        done()
      .fail (err) => console.log err

  it 'should return book properly', (done) =>
    Q.invoke(Borrowings, 'returnBook', 1)
      .then (result) =>
        expect(moment(result[0].returnDate).format('YYYYMMDD')).eql moment().format('YYYYMMDD')
        done()
      .fail (err) => console.log err

  it 'should return empty array for not found', (done) =>
    Q.invoke(Borrowings, 'get', {borrowId: 'should not be found'})
      .then (resultGet) =>
        expect(resultGet).eql []
        done()
      .fail (err) => console.log err


