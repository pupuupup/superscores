SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `Books` (
  `bookId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `edition` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `addedDate` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`bookId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `Books` (`bookId`, `title`, `year`, `edition`, `publisher`, `author`, `addedDate`, `createdAt`, `updatedAt`) VALUES
(1, 'Lord of the ling', 2015, '1', 'Super Store', 'Yhom Man', '2015-11-25 00:00:00', '2015-11-25 00:00:00', '2015-11-25 00:00:00'),
(2, 'T Ded 99', 2012, '2', 'DKP', 'Yhom Boy', '2015-11-25 00:00:00', '2015-11-25 00:00:00', '2015-11-25 00:00:00');

CREATE TABLE `Borrowing` (
  `borrowId` int(11) NOT NULL AUTO_INCREMENT,
  `borrowDate` datetime DEFAULT NULL,
  `returnDeadline` datetime DEFAULT NULL,
  `returnDate` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `memberId` int(11) DEFAULT NULL,
  `bookId` int(11) DEFAULT NULL,
  `staffId` int(11) DEFAULT NULL,
  PRIMARY KEY (`borrowId`),
  KEY `memberId` (`memberId`),
  KEY `bookId` (`bookId`),
  KEY `staffId` (`staffId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `Members` (
  `memberId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`memberId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `Members` (`memberId`, `firstName`, `lastName`, `createdAt`, `updatedAt`) VALUES
(1, 'Boy', 'Imagine', '2015-11-25 00:00:00', '2015-11-25 00:00:00'),
(2, 'Singtho', 'Numchok', '2015-11-25 00:00:00', '2015-11-25 00:00:00');

CREATE TABLE `Staffs` (
  `staffId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`staffId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `Staffs` (`staffId`, `firstName`, `lastName`, `createdAt`, `updatedAt`) VALUES
(1, 'Peter', 'Pan', '2015-11-25 00:00:00', '2015-11-25 00:00:00'),
(2, 'Max', 'Yom', '2015-11-25 00:00:00', '2015-11-25 00:00:00');

ALTER TABLE `Borrowing`
  ADD CONSTRAINT `borrowing_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `Members` (`memberId`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `borrowing_ibfk_2` FOREIGN KEY (`bookId`) REFERENCES `Books` (`bookId`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `borrowing_ibfk_3` FOREIGN KEY (`staffId`) REFERENCES `Staffs` (`staffId`) ON DELETE SET NULL ON UPDATE CASCADE;
