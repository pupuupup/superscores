fs = require 'fs'
Request = require('supertest')
app = require('../app')()
file = fs.readFileSync(__dirname + '/fixtures/library.sql').toString()

before (done) ->
  global.request = Request(app.server)
  sql.queryAsync("DROP DATABASE #{config.mysql.database};
                  CREATE DATABASE #{config.mysql.database};
                  use #{config.mysql.database};")
    .then => sql.queryAsync(file)
    .then => done()
    .catch (err) => console.log err
