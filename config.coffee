_ = require 'lodash'
env = process.env.NODE_ENV or 'development'
process.env.NODE_ENV = env

config =
  default: {}
  development:
    mysql:
      host: 'localhost',
      user: 'ssadmin',
      password: 'P@ssw0rd',
      database: 'superscores'
  test:
    mysql:
      host: 'localhost',
      user: 'ssadmin',
      password: 'P@ssw0rd',
      database: 'superscores_test'


config[env] = config.development unless config[env]?
module.exports = _.merge config['default'], config[env]
